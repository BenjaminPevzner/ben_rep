#pragma once


#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "TV_CUDA_Util.cuh"




///////////////////////////////////////////////////////////////////////////////////////////////////
// TV_CUDA_Timer helps to measure multiple times using CUDA events and accumulate them.
//	 Use Reset to start a new session.
//	 Then use Start()/Stop() for each measurement.
//	 Use GetLastTimeMS(), GetTotalTimeMS() to get the info.
class TV_CUDA_Timer
{
public:
	TV_CUDA_Timer()
	{
		m_Event1 = NULL;
		m_Event2 = NULL;
		m_lastTimeMS = 0;
		m_totalTimeMS = 0;
		m_bStarted = false;
	}

	~TV_CUDA_Timer()
	{
		Deinit();
	}

	void Init()
	{
		cudaError_t cres = cudaEventCreate(&m_Event1);
		TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
		cres = cudaEventCreate(&m_Event2);
		TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
	}

	void Deinit()
	{
		cudaError_t cres;
		if (m_Event1 != NULL)
		{
			cres = cudaEventDestroy(m_Event1);
			m_Event1 = NULL;
			TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
		}
		if (m_Event2 != NULL)
		{
			cres = cudaEventDestroy(m_Event2);
			m_Event2 = NULL;
			TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
		}
	}

	void Reset()
	{
		TV_CUDA_GLUECODE_CHECK_ASSERT(m_bStarted == false);
		m_totalTimeMS = 0;
	}

	void Start()
	{
		TV_CUDA_GLUECODE_CHECK_ASSERT(m_bStarted == false);
		m_bStarted = true;
		cudaError_t cres = cudaEventRecord(m_Event1);
		TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
	}

	void Stop(char const * pMeasurementName)	// if pMeasurementName==NULL no msg will be printed
	{
		TV_CUDA_GLUECODE_CHECK_ASSERT(m_bStarted == true);
		m_bStarted = false;
		cudaError_t cres = cudaEventRecord(m_Event2);
		TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
		cres = cudaEventSynchronize(m_Event2);
		TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
		cres = cudaEventElapsedTime(&m_lastTimeMS, m_Event1, m_Event2);
		TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cres);
		m_totalTimeMS += m_lastTimeMS;

		if (pMeasurementName)
		{
			TV_CUDA_GLUECODE_PRINTF("### %s   Time (cuda events) (milisecs): %f\n\n", pMeasurementName, GetLastTimeMS());
		}
	}

	float GetLastTimeMS()
	{
		return m_lastTimeMS;
	}

	float GetTotalTimeMS()
	{
		return m_totalTimeMS;
	}

protected:
	cudaEvent_t m_Event1;
	cudaEvent_t m_Event2;
	float		m_totalTimeMS;
	float		m_lastTimeMS;
	bool		m_bStarted;
};
