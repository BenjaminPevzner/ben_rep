#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "TV_CUDA_Defines.cuh"

#include <stdio.h>




// Comment out the following line to disable checking local mem size.
// NOTE!!! if you disable it and there is not enough local mem, the program will behave in an undefined way !!!
#define VALIDATE_LOCAL_MEM_SIZE



__device__ __forceinline__ bool IsBlockIdxAndThreadIdx(int blockIdxX, int blockIdxY, int threadIdxX, int threadIdxY)
{
	return (blockIdx.x == blockIdxX && blockIdx.y == blockIdxY && threadIdx.x == threadIdxX && threadIdx.y == threadIdxY);
}

__device__ __forceinline__ bool IsBlockIdx00AndThreadIdx00()
{
	return IsBlockIdxAndThreadIdx(0, 0, 0, 0);
}



// NOTE: this function also updates LocalMemPoolOffset according to the new reuired size.
__device__ __forceinline__ bool ValidateLocalMemSizeFromPoolWithUserData(int PoolSize, unsigned char * pLocalMemPool, char const * pKernelName, char const * pMemName, int RequiredBytes, int LocalMemPoolOffset, int userData)
{
#ifdef VALIDATE_LOCAL_MEM_SIZE
	int BytesLeftInPool = PoolSize - LocalMemPoolOffset;

	if (IsBlockIdx00AndThreadIdx00())
	{
		TV_CUDA_KERNEL_PRINTF("block:%d,%d    thread:%d,%d     Bytes left in pool: %6d\t     Required bytes: %6d\t  addr:0x%llx [%s,%s]  (userData:%d)\n", blockIdx.x, blockIdx.y, threadIdx.x, threadIdx.y, BytesLeftInPool, RequiredBytes, pLocalMemPool + LocalMemPoolOffset, pKernelName, pMemName, userData);
	}
	if (RequiredBytes >  BytesLeftInPool)
	{
		//if (IsBlockIdx00AndThreadIdx00())
		{
			TV_CUDA_KERNEL_ERROR_PRINTF("!!!*** block:%d,%d    thread:%d,%d   Local mem: [%s,%s]  POOL TOO SMALL !!! Required:%d    Left:%d  (userData:%d)\n", blockIdx.x, blockIdx.y, threadIdx.x, threadIdx.y, pKernelName, pMemName, RequiredBytes, BytesLeftInPool, userData);
		}
		return false;
	}
	return true;

#else
	return true;
#endif
}


// NOTE: this function also updates LocalMemPoolOffset according to the new reuired size.
__device__ __forceinline__ bool ValidateLocalMemSizeFromPool(int PoolSize, unsigned char * pLocalMemPool, char const * pKernelName, char const * pMemName, int RequiredBytes, int LocalMemPoolOffset)
{
	return ValidateLocalMemSizeFromPoolWithUserData(PoolSize, pLocalMemPool, pKernelName, pMemName, RequiredBytes, LocalMemPoolOffset, 0);
}



#define TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(ans) { TV_CUDA_GLUECODE_GpuErrCheckAssertFunc((ans), __FILE__, __LINE__); }
inline void TV_CUDA_GLUECODE_GpuErrCheckAssertFunc(cudaError_t code, const char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		TV_CUDA_KERNEL_ERROR_PRINTF("GPUassert [%s line %d]: %s\n", file, line, cudaGetErrorString(code));
		if (abort)
		{
			TV_CUDA_KERNEL_ERROR_PRINTF("Press <ENTER>");
			getchar();
			exit(code);
		}
	}
}



#define TV_CUDA_GLUECODE_CHECK_ASSERT(b) { TV_CUDA_GLUECODE_CheckAssertFunc((b), #b, __FILE__, __LINE__); }
inline void TV_CUDA_GLUECODE_CheckAssertFunc(bool b, const char * msg, const char *file, int line, bool abort = true)
{
	if (!b)
	{
		TV_CUDA_KERNEL_ERROR_PRINTF("assert [%s line %d]: %s\n", file, line, msg);
		if (abort)
		{
			TV_CUDA_KERNEL_ERROR_PRINTF("Press <ENTER>");
			getchar();
			exit(0);
		}
	}
}



#define TV_CUDA_KERNEL_CHECK_ASSERT(b) { TV_CUDA_KERNEL_CheckAssertFunc((b), #b, __FILE__, __LINE__); }
__device__ __forceinline__ void TV_CUDA_KERNEL_CheckAssertFunc(bool b, const char * msg, const char *file, int line)
{
	if (!b)
	{
		TV_CUDA_KERNEL_ERROR_PRINTF("GPU Kernel Assert FAILED [%s line %d]: %s\n", file, line, msg);
	}
}


__device__ __forceinline__
unsigned char * GetPtrIntoGray8BitImage(int Stride, unsigned char * pImg, int x, int y)
{
	return pImg + y*Stride + x;
}

__device__ __forceinline__
unsigned short * GetPtrIntoGray16BitImage(int Stride, unsigned char * pImg, int x, int y)
{
	unsigned char * pTmp = pImg + y*Stride + x * 2;
	return (unsigned short *)pTmp;
}


__device__ __forceinline__
short * GetPtrIntoGray16BitSignedImage(int Stride, unsigned char * pImg, int x, int y)
{
	unsigned char * pTmp = pImg + y*Stride + x * 2;
	return (short *)pTmp;
}



__device__ __forceinline__ unsigned char GetUnsigned8bitImgPixelVal(unsigned char * pImg, int Stride, int x, int y)
{
	return pImg[y * Stride + x];
}


__device__ __forceinline__ void SetUnsigned8bitImgPixelVal(unsigned char * pImg, int Stride, int x, int y, unsigned char Val)
{
	pImg[y * Stride + x] = Val;
}


__device__ __forceinline__ short GetSigned16bitImgPixelVal(unsigned char * pImg, int StrideBytes, int x, int y)
{
	unsigned char * pTmp = pImg + y*StrideBytes + x * 2;
	return *(short*)pTmp;
}


__device__ __forceinline__ void SetSigned16bitImgPixelVal(unsigned char * pImg, int StrideBytes, int x, int y, short Val)
{
	unsigned char * pTmp = pImg + y*StrideBytes + x * 2;
	*(short*)pTmp = Val;
}



__device__ __forceinline__ void ThreadsCopyToLocalMem(unsigned char * pLocalMem, int LocalW, int LocalH, int LocalStride, unsigned char * pGlobalMem, int GlobalMemStride, int GlobalMemX1, int GlobalMemY1)
{
	int NumBytesPerThread = 4;
	int ThreadStrideX = blockDim.x*NumBytesPerThread;
	int ThreadStrideY = blockDim.y;

	for (int y = threadIdx.y; y < LocalH; y += ThreadStrideY)
	{
		for (int x = threadIdx.x*NumBytesPerThread; x < LocalW; x += ThreadStrideX)
		{
			int GlobalX = GlobalMemX1 + x;
			int GlobalY = GlobalMemY1 + y;

			int LocalX_InPatch = x;
			int LocalY_InPatch = y;

			int GlobalIdx = GlobalY * GlobalMemStride + GlobalX;
			int LocalIdx_InPatch = LocalY_InPatch * LocalStride + LocalX_InPatch;

			int * pLocal4Bytes = (int*)(pLocalMem + LocalIdx_InPatch);
			int * pGlobal4Bytes = (int*)(pGlobalMem + GlobalIdx);
			*pLocal4Bytes = *pGlobal4Bytes;
		}
	}
}



template <class T>
__device__ __forceinline__ void TV_BubbleSortAscendingInPlace(short numValues, T * pValues)
{
	for (int i = 0; i < numValues - 1; ++i)
	{
		for (int j = i + 1; j < numValues; ++j)
		{
			if (pValues[i] > pValues[j])
			{
				T t = pValues[i];
				pValues[i] = pValues[j];
				pValues[j] = t;
			}
		}
	}
}


template <class T>
__device__ __forceinline__ void TV_MakeUniqueInPlace(short & numValues, T * pValues)
{
	int idx = 0;
	while (idx < numValues - 1)
	{
		int idx2 = idx;
		while (idx2 + 1 < numValues && pValues[idx] == pValues[idx2 + 1])
		{
			idx2++;
		}

		if (idx != idx2)
		{
			int skip = idx2 - idx;
			for (int j = idx + 1; j < numValues - skip; ++j)
			{
				pValues[j] = pValues[j + skip];
			}
			numValues -= skip;
		}

		idx++;
	}
}

template <class T>
__device__ __forceinline__ void TV_BubbleSortAscendingAndMakeUniqueInPlace(short & numValues, T * pValues)
{
	// Sort:
	TV_BubbleSortAscendingInPlace(numValues, pValues);

	// Make unique:
	TV_MakeUniqueInPlace(numValues, pValues);
}



template <class T>
__device__ __forceinline__ void TV_RemoveValueInPlace(short & numValues, T * pValues, T valueToRemove)
{
	int idxOut = 0;
	for (int i = 0; i < numValues; i++)
	{
		if (pValues[i] != valueToRemove)
		{
			pValues[idxOut++] = pValues[i];
		}
	}
	numValues = idxOut;
}
