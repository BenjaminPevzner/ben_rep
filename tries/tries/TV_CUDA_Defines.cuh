#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"


// Comment out the following line to disable all info kernel printfs
//#define DO_KERNEL_PRINTF

// Comment out the following line to disable all ERROR kernel printfs
#define DO_KERNEL_ERROR_PRINTF

// Comment out the following line to disable all glue code printfs (printfs in code that launches the kernels etc.)
#define DO_GLUECODE_PRINTF

// Comment out the following line to disable all ERROR glue code printfs:
#define DO_GLUECODE_ERROR_PRINTF





#ifdef DO_KERNEL_PRINTF
#define TV_CUDA_KERNEL_PRINTF(f_, ...)  printf((f_), __VA_ARGS__)
#else
#define TV_CUDA_KERNEL_PRINTF(f_, ...)
#endif // #ifdef DO_KERNEL_PRINTF



#ifdef DO_KERNEL_ERROR_PRINTF
#define TV_CUDA_KERNEL_ERROR_PRINTF(f_, ...)  printf((f_), __VA_ARGS__)
#else
#define TV_CUDA_KERNEL_ERROR_PRINTF(f_, ...)
#endif // #ifdef DO_KERNEL_ERROR_PRINTF




#ifdef DO_GLUECODE_PRINTF
#define TV_CUDA_GLUECODE_PRINTF(f_, ...)  printf((f_), __VA_ARGS__)
#else
#define TV_CUDA_GLUECODE_PRINTF(f_, ...)
#endif // #ifdef DO_GLUECODE_PRINTF


#ifdef DO_GLUECODE_ERROR_PRINTF
#define TV_CUDA_GLUECODE_ERROR_PRINTF(f_, ...)  printf((f_), __VA_ARGS__)
#else
#define TV_CUDA_GLUECODE_ERROR_PRINTF(f_, ...)
#endif // #ifdef DO_GLUECODE_ERROR_PRINTF




static const int CALC_DISP_Y_ANCHORS_INTERVAL_FULL = 32;// 32;//32//1//32

static const int CALC_TILED_DISP_SEARCHRANGE_NEIGHBOURHOOD_HALF_SIZE = 48;

static const int CALC_TILED_DISP_MIN_SEARCH_RANGE_INTERVAL = 12;

static const int  CALC_TILED_SEARCH_TANGE_BLOCK_PROCESS_PIXELS_X = 64;//85//230//90//140	



#define INVALID_DISP_X_VALUE (-9999)

#define  POINT_3D_COORD_INVALID (-99999)

#define  STDDEV_EPSILON (0.000001f)


// !!!!! you cannot change the patch size without changing hard-coded code marked with DEPENDENT_ON_PATCH_SIZE !!!!!
const int PATCH_HALF_W = 8;	// Patch size W is (PATCH_HALF_W * 2 + 1)
const int PATCH_HALF_H = 8;	// Patch size H is (PATCH_HALF_H * 2 + 1)
// !!!!! you cannot change the patch size without changing hard-coded code marked with DEPENDENT_ON_TILE_PATCH_SIZE !!!!!
const int TILE_PATCH_HALF_W = 5;
const int TILE_PATCH_HALF_H = 5;


// Conf and max cost:
__device__ const float MAX_SAD_FOR_GOOD_CANDIDATE = 190;// 60;
__device__ const float MIN_SAD_DIFF_BETWEEN_BEST_AND_SECOND_BEST = 60; // -1;// 8.f;// -1;// 8.f;// 1.1f;// -1;// 1.2;
__device__ const float TILE_MAX_SAD_FOR_GOOD_CANDIDATE = 85.f; // 999;// 60;
__device__ const float TILE_MAX_SAD_FOR_GOOD_CANDIDATE_FROM_DECIMATED_ANCHOR = 95.f;
__device__ const float TILE_MIN_SAD_DIFF_BETWEEN_BEST_AND_SECOND_BEST = 10; // -1;// 4.f;// 1.1f;// -1;// 1.2;


// Sobel:
const int CALC_SOBEL_SUM_THRESHOLD = 20;


// Minimum number of anchors for tiling:
static const short CALC_TILED_DISP_MIN_ANCHORS_TOTAL = 16;
static const short CALC_TILED_DISP_MIN_ANCHORS_PER_ANCHOR_LINE = 1;


static const short MIN_DISP_X_INVALID = 8888;		// NOTE: must be BIGGER  than any valid disparity value !!!
static const short MAX_DISP_X_INVALID = -8888;		// NOTE: must be SMALLER than any valid disparity value !!!



static const int MIN_DIST_X_FOR_NON_MAXIMAL_SUPPRESSION = 2;

