
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "TV_CUDA_Timer.cuh"
#include <stdio.h>


#pragma warning (push)
#pragma warning (disable: 4793)	// GILNOTE: disable the following warning: warning C4793: ...: function compiled as native:   	Found an intrinsic not supported in managed code
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#pragma warning (pop)
using namespace cv;


//cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);



__global__ void dialate(unsigned char * d_data, 
						const int numRows,
						const int numCols,
						unsigned char * d_outputdata,
						const int blocksize)
{
	/* shared memory solution
	int x = threadIdx.x;
	int y = blockIdx.x;
	unsigned char temp = 0;
	extern __shared__ unsigned char  sharedData[];
	//3*x=position 3*x+1=row above 3*x+2 = row below
	sharedData[3 * x] = d_data[x + y*numCols];
	if (y > 0)
		sharedData[3 * x + 1] = d_data[x + (y - 1)*numCols];
	else
		sharedData[3 * x + 1] = 0;
	if (y == numRows - 1)
		sharedData[3 * x + 2] = 0;
	else
		sharedData[3 * x + 2] = d_data[x + (y + 1)*numCols];

	syncthreads();
	temp = MAX(sharedData[3 * x], sharedData[3 * x + 1]);
	temp = MAX(temp, sharedData[3 * x + 2]);
	if (x > 0)
	{
		temp = MAX(temp, sharedData[3 * x - 3]);
		temp = MAX(temp, sharedData[3 * x - 2]);
		temp = MAX(temp, sharedData[3 * x - 1]);
	}
	if (x < numCols - 1)
	{
		temp = MAX(temp, sharedData[3 * x + 3]);
		temp = MAX(temp, sharedData[3 * x + 4]);
		temp = MAX(temp, sharedData[3 * x + 5]);
	}

	d_outputdata[x + y*numCols] = temp;

	*/
	int temp = 0;
	int threadindex = threadIdx.x;
	int blockindex_X = blockIdx.x;
	int blockindex_Y = blockIdx.y;
	if (threadindex+blockindex_X * blockDim.x < numCols)
		{
			temp = d_data[threadindex + blockindex_X * blocksize + blockindex_Y * numCols];
			if (blockindex_Y > 0)
			{
				temp = MAX(temp, d_data[threadindex + blockindex_X * blocksize + (blockindex_Y - 1) * numCols]);
				
				if (threadindex + blockindex_X * blocksize < numCols - 1)
				{
					temp = MAX(temp, d_data[threadindex + 1 + blockindex_X * blocksize + (blockindex_Y - 1) * numCols]);
				}
				if (threadindex + blockindex_X * blocksize > 0)
				{
					temp = MAX(temp, d_data[threadindex - 1 + blockindex_X * blocksize + (blockindex_Y - 1) * numCols]);
				}
			}
			if (blockindex_Y < numRows - 1)
			{
				temp = MAX(temp, d_data[threadindex + blockindex_X * blocksize + (blockindex_Y + 1) * numCols]);
			
				if (threadindex + blockindex_X * blocksize < numCols - 1)
				{
					temp = MAX(temp, d_data[threadindex + 1 + blockindex_X * blocksize + (blockindex_Y + 1) * numCols]);
				}
				if (threadindex + blockindex_X * blocksize > 0)
					temp = MAX(temp, d_data[threadindex - 1 + blockindex_X * blocksize + (blockindex_Y + 1) * numCols]);
			}
			if (threadindex + blockindex_X * blocksize < numCols - 1)
			{
				temp = MAX(temp, d_data[threadindex + 1 + blockindex_X * blocksize + blockindex_Y * numCols]);
			}
			if (threadindex + blockindex_X * blocksize > 0)
			{
				temp = MAX(temp, d_data[threadindex - 1 + blockindex_X * blocksize + blockindex_Y * numCols]);
			}
			//Finished result gets copied into output file
			d_outputdata[threadindex + blockindex_X * blocksize + blockindex_Y * numCols] = temp;
		}
	};

class TV_CUDA_Proc
{
public:
	TV_CUDA_Proc();
	~TV_CUDA_Proc();
	void Deinit();
	bool Init(unsigned int size);
	bool CudaDoWork(unsigned char * data, const int rowsize, const int colsize, const int size);
	void CheckErrorAndDevSynchronize();

	TV_CUDA_Timer m_KernelTimer;
	unsigned char * d_data ;
	unsigned char * d_outputdata ;

protected:
	//memory host to device & device to host
	bool MemoryManageH2D(unsigned char  * hostBuf, unsigned char * devBuf, unsigned int size);
	bool MemoryManageD2H(unsigned char * hostBuf, unsigned char * devBuf, unsigned int size);
	bool MemoryAlloc(void**  devBuf, unsigned int size);
	bool MemoryFree(unsigned char ** buff);
	void TimerStart(TV_CUDA_Timer  KernelTimer);
	void TimerStop(TV_CUDA_Timer  KernelTimer);
};

TV_CUDA_Proc::TV_CUDA_Proc()
{
	d_data = 0;
	d_outputdata = 0;
}

TV_CUDA_Proc::~TV_CUDA_Proc()
{
	Deinit();
}

bool TV_CUDA_Proc::Init(unsigned int size)
{
	cudaError_t cudaStatus;
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) 
	{
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		return false;
	}
	m_KernelTimer.Init();
	if (!MemoryAlloc((void**)&d_data, size))
	{
		fprintf(stderr, "cudaMalloc d_data failed  \n" );
	}

	if (!MemoryAlloc((void**)&d_outputdata, size))
	{
		fprintf(stderr, "cudaMalloc d_outputdata failed");
	}
	cudaMemset(d_outputdata, 128, size);
	return true;
};


void TV_CUDA_Proc::Deinit()
{
	cudaError_t cudaStatus;
	m_KernelTimer.Deinit();
	if (!MemoryFree(&d_data))
	{
		fprintf(stderr, "d_data not freed");
	}
	if (!MemoryFree(&d_outputdata))
	{
		fprintf(stderr, "cudafree d_outputdata failed\n");
	}

	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaDeviceReset failed!");
	}
	TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cudaStatus);
	}

bool TV_CUDA_Proc::CudaDoWork(unsigned char * h_data, const int numrows, const int numcols, const int size)
{

	dim3 gridsize  (numcols/500+1, numrows, 1);
	const dim3 blocksize  (500, 1, 1);
	int MAXBLOCKSIZE = 500;
	if ( !MemoryManageH2D(h_data, d_data, size))
	{
		fprintf(stderr, "cudaMemcpyH2D h_data -> d_data failed  \n");
	}
	for (int i = 0; i < 5; i++)
	{
		m_KernelTimer.Start();
		dialate << <gridsize, blocksize >> > (d_data, numrows, numcols, d_outputdata, MAXBLOCKSIZE);
		//dialate <<< gridsize, blocksize,3*numcols*sizeof(unsigned char) >>> (d_data,numrows,numcols,d_outputdata,blocksize);
		m_KernelTimer.Stop("b");
		CheckErrorAndDevSynchronize();
	}

	if (!MemoryManageD2H(h_data, d_outputdata, size))
	{
		fprintf(stderr, "cudaMemcpyD2H d_outputdata failed  \n");
	}
	return true;
};

bool TV_CUDA_Proc::MemoryManageH2D(unsigned char * hostBuf, unsigned char * devBuf, unsigned int size)
{
	cudaError_t cudaStatus;
	cudaStatus = cudaMemcpy(devBuf, hostBuf, size, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpyH2D failed  %s\n", cudaGetErrorString(cudaStatus) );
		return false;
	}
	return true;
};

bool TV_CUDA_Proc::MemoryManageD2H(unsigned char * hostBuf, unsigned char * devBuf, unsigned int size)
{
	cudaError_t cudaStatus;
	cudaStatus = cudaMemcpy(hostBuf, devBuf, size, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) 
	{
		fprintf(stderr, "cudaMemcpyD2H failed  %s\n", cudaGetErrorString(cudaStatus));
		return false;
	}
	return true;
}

bool TV_CUDA_Proc::MemoryAlloc(void**  devBuf, unsigned int size)
{
	cudaError_t cudaStatus;
	cudaStatus = cudaMalloc((void**)devBuf, size);
	if(cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed  %s\n", cudaGetErrorString(cudaStatus));
		return false;
	}
	return true;
}

bool TV_CUDA_Proc::MemoryFree(unsigned char * *buff)
{
	cudaError_t cudaStatus;
	if (*buff != NULL)
	{
		cudaStatus = cudaFree((void*)*buff);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cuda free fail\n");
			return false;
		}
		*buff = NULL;
	}
	return true;
};

void TV_CUDA_Proc::CheckErrorAndDevSynchronize()
{
	cudaError_t cudaStatus;
	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cudaStatus);
	// Wait for device to finish everything:
	cudaStatus = cudaDeviceSynchronize();
	TV_CUDA_GLUECODE_GPU_ERR_CHECK_ASSERT(cudaStatus);
}


void TV_CUDA_Proc::TimerStart(TV_CUDA_Timer  m_KernelTimer)
{
	m_KernelTimer.Start();
};
void TV_CUDA_Proc:: TimerStop(TV_CUDA_Timer  m_KernelTimer)
{
	m_KernelTimer.Stop("Timer:");
};
int maincuda()
{
	TV_CUDA_Proc proc;
	cv::Mat img(10000, 10000, CV_8UC1, Scalar(0));
	for (int j = 5; j < 50; j++)
	{
	
		for (int i = 400; i < 1000; i++)
		{
			*(img.data + img.step[0] * j + i) = 254;
		}
	}
	const int numrows = img.rows;
	const int numcols = img.cols;
	const int size = numrows*numcols * sizeof(unsigned char);
	
	imwrite("c:\\micha\\input.png",img);
	if (!proc.Init(size))
	{
		fprintf(stderr, "init failed: \n" );
		return 1;
	};
	if (!proc.CudaDoWork(img.data, numrows, numcols, size))
	{
		fprintf(stderr, "CudaWork Failed: \n");
		return 1;
	};
	proc.Deinit();
	imwrite("c:\\micha\\output.png", img);
	return 0;
}